#' Plot phenomaster data in dependance of time
#'
#' `plot.phenodata()` plots phenomaster data without grouping.
#' `plot.phenodata.summary()` plots mean +- SEM over all individuals in one group.
#'
#' @return A beatiful plot.
#' @param df A tibble holding the data from a phenomaster run as produced by
#'    [read_calorimetry()].
#' @param y Name of a column in df. Holds values for plotting.
#' @param condition1 Name of a column in df. Used for grouping by colour.
#' @param condition2 Name of a column in df. Used for facetting. If NULL, no
#'    facetting will be done.
#' @param time_variable
#' @param outlier
#' @param y_lab y axis label.
#' @param col_lab color scale label.
#' @param pch_lab
#' @param width
#' @param base_size
#' @param base_family
#' @export
plot_phenodata_stats_time <- function(df, y, condition1, condition2 = NULL,
    time_variable, outlier = NULL,
    y_lab, col_lab = NULL, pch_lab = NULL, width = .25,
    base_size = 8, base_family = "Helvetica") {

    df_stats <- get_df_stats(df,
        y, outlier,
        groups = c("AnimalNo", time_variable, condition1, condition2))


    pl <- df_stats %>%
        ggplot2::ggplot(ggplot2::aes(get(time_variable), y_mean)) +
        ggiraphe::geom_point_interactive(
            ggplot2::aes(colour = get(condition1),
                pch = get(condition2),
                tooltip = glue::glue("AnimalNo: {AnimalNo}"),
                data_id = AnimalNo),
            size = 1,
            alpha = .5,
            position = ggplot2::position_dodge(width = width)) +
        ggplot2::stat_summary(
            ggplot2::aes(colour = get(condition1), pch = get(condition2)),
            fun = ggplot2::mean, geom = "crossbar",
            position = ggplot2::position_dodge(width = width),
            width = width,
            fatten = 1.5, lwd = .25) +
        ggplot2::stat_summary(
            ggplot2::aes(colour = genotype, pch = get(condition2)),
            geom = "errorbar", fun.data = ggplot2::mean_se,
            width = width * .666,
            lwd = .25,
            position = ggplot2::position_dodge(width = width)) +
        ggplot2::stat_summary(
            ggplot2::aes(colour = genotype, lty = get(condition2)),
            fun = ggplot2::mean, geom = "line", lwd = .25,
            position = ggplot2::position_dodge(width = width))     +
        ggthemes::scale_colour_colorblind(name = col_lab) +
        ggthemes::theme_tufte(base_size = base_size, base_family = base_family) +
        ggthemes::geom_rangeframe() +
        ggplot2::xlab("Time since start of cold treatment [d]") +
        ggplot2::labs(y = y_lab, pch = pch_lab, lty = pch_lab)
}
