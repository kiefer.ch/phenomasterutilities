#' Calculate summary statistics
#'
#' `get_df_stats()` calculates summary values for plotting.
#'
#' @param df A tibble holding the data from a phenomaster run as produced by
#'    [read_calorimetry()].
#' @param groups Groups for plotting.
#' @param y Name of a column in df. Holds values for plotting.
#' @param outlier
#'
#' @export
get_df_stats <- function(df, groups, y, outlier = NULL) {

    if(!is.null(outlier)) {

        df <- df %>%
            dplyr::filter(get(outlier) == FALSE)

    }

    df %>%
        dplyr::group_by_at(vars(matches(groups))) %>%
        dplyr::summarise(
            y_mean = mean(get(y), na.rm = TRUE),
            weight = unique(`Weight_start`)) %>%
        dplyr::ungroup() %>%
        tidyr::drop_na()
}


#' Plot phenomaster data
#'
#' `plot.phenodata()` plots phenomaster data without grouping.
#' `plot.phenodata.summary()` plots mean +- SEM over all individuals in one group.
#'
#' @param df A tibble holding the data from a phenomaster run as produced by
#'    [read_calorimetry()].
#' @param y Name of a column in df. Holds values for plotting.
#' @param condition1 Name of a column in df. Used for grouping by colour.
#' @param condition2 Name of a column in df. Used for facetting. If NULL, no
#'    facetting will be done.
#' @param facet_cols
#' @param facet_rows
#' @param outlier
#' @param y_lab y axis label.
#' @param col_lab color scale label.
#' @param pch_lab
#' @param interactive
#'
#' @return A beatiful plot.
#'
#' @export
plot_phenodata_stats <- function(df, y, condition1, condition2 = NULL,
    facet_cols = NULL, facet_rows = NULL, outlier = NULL,
    y_lab, col_lab, pch_lab, interactive = FALSE) {

    df_stats <- get_df_stats(df, y, outlier,
        groups = c("AnimalNo", condition1, condition2, facet_cols, facet_rows))

    pl <- df_stats %>%
        ggplot2::ggplot(ggplot2::aes(weight, y_mean)) +
        ggiraphe::geom_point_interactive(
            ggplot2::aes(colour = get(condition1),
                pch = get(condition2),
                tooltip = glue::glue("AnimalNo: {AnimalNo}\n"),
                data_id = AnimalNo)) +
        ggplot2::geom_smooth(
            ggplot2::aes(colour = get(condition1), lty = get(condition2)),
            method = "lm",
            se = FALSE,
            size = .5) +
        ggthemes::scale_colour_colorblind(name = col_lab) +
        ggthemes::theme_tufte(base_size = 12, base_family = "Helvetica") +
        ggthemes::geom_rangeframe() +
        ggplot2::xlab("Weight [g]") +
        ggplot2::labs(y = y_lab, pch = pch_lab, lty = pch_lab)

    if(!is.null(facet_cols)) {

        if (is.null(facet_rows)) {

            pl <- pl +
                ggplot2::facet_grid(cols = dplyr::vars(get(facet_cols)))

        } else {

            pl <- pl +
                ggplot2::facet_grid(cols = dplyr::vars(get(facet_cols)),
                    rows = dplyr::vars(get(facet_rows)))
        }

    }



    if (interactive) {

        pl <- ggiraph::girafe(ggobj = pl,
                width_svg = 10, height_svg = 6.18,
                options = list(
                    ggiraph::opts_sizing(rescale = FALSE),
                    ggiraph::opts_hover(css = "fill:red;stroke:black;"),
                    ggiraph::opts_hover_inv(css = "opacity:0.3;"),
                    ggiraph::opts_zoom(max = 5)))
    }

    pl

}


print_anova <- function(mod) {
    mod %>%
        tibble::as_tibble(rownames = "coefficient") %>%
        dplyr::mutate(
            signif = symnum(`Pr(>F)`,
                cutpoints = c(0, .001, .01, .05, .1, 1),
                symbols = c("***", "**", "*", ".", " "))) %>%
        knitr::kable(format = "html") %>%
        kableExtra::kable_styling()
}


print_coefficients <- function(mod) {
    mod$coefficients %>%
        tibble::as_tibble(rownames = "coefficient") %>%
        dplyr::mutate(
            signif = symnum(`Pr(>|t|)`,
                cutpoints = c(0, .001, .01, .05, .1, 1),
                symbols = c("***", "**", "*", ".", " "))) %>%
        knitr::kable(format = "html") %>%
        kableExtra::kable_styling()
}
