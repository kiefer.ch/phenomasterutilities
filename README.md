
# phenomasterUtilities

<!-- badges: start -->
<!-- badges: end -->

Utility functions for analysing phenomaster experiments.

## Installation

Can be installed using devtools.

``` r
devtools::install_gitlab("kiefer.ch/phenomasterutilities")
```
